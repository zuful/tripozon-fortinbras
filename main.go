package main

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
	"log"
	"net/http"
	"time"
)

const (
	port = ":50051"
)

func main() {

	r := gin.Default()
	// CORS for https://foo.com and https://github.com origins, allowing:
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours

	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{
			"http://localhost:8100",
			"http://localhost:4200",
			"https://messenger-1670c.firebaseapp.com",
			"https://tripozon-pro-admin.firebaseapp.com",
			"https://tripozon-pro.firebaseapp.com"},
		AllowMethods:     []string{"PUT", "PATCH"},
		AllowHeaders:     []string{"Origin", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length", "Content-Type"},
		AllowCredentials: true,
		/*AllowOriginFunc: func(origin string) bool {
			return origin == "https://github.com"
		},*/
		MaxAge: 12 * time.Hour,
	}))

	//r.Use(cors.Default())//allows all origins (must but changed when put in production)

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	//-----------------------------------ESTABLISHMENTS
	r.GET("/search-establishments/:category/:city", func(c *gin.Context) {

		var category string = c.Param("category")
		var city string = c.Param("city")

		var allSearchedShops []*EstablishmentStructure = getSearchedEstablishment(category, city)
		fmt.Println("%v", allSearchedShops)
		c.JSON(200, allSearchedShops)
	})

	r.GET("/establishment/:establishmentId", func(c *gin.Context) {

		var establishmentId string = c.Param("establishmentId")
		var establishment *EstablishmentStructure = getEstablishment(establishmentId)

		c.JSON(200, establishment)
	})

	r.POST("/establishment", func(c *gin.Context) { //todo : additional security -> ask for a valid token

		var establishment *EstablishmentStructure

		err := c.Bind(&establishment)
		if err != nil {
			println("inside")
			log.Println(err)
		}

		establishment.Id = uuid.NewV4().String()
		setEstablishmentGeoloc(establishment)

		res := createEstablishment(establishment)

		c.JSON(200, res)
	})

	r.PATCH("/establishment/:establishmentId", func(c *gin.Context) {

		var establishmentId string = c.Param("establishmentId")
		var establishment *EstablishmentStructure

		err := c.Bind(&establishment)
		if err != nil {
			log.Println(err)
		}

		res := updatEstablishment(establishmentId, establishment)

		c.JSON(200, res)
	})

	//-----------------------------------Employee
	r.GET("/all-employees/:establishmentId", func(c *gin.Context) {

		var establishmentId string = c.Param("establishmentId")
		var allEmployees []EmployeeStructure = getAllEmployees(establishmentId)

		c.JSON(200, allEmployees)
	})

	r.GET("/employee/:establishmentId/:employeeId", func(c *gin.Context) {

		var establishmentId string = c.Param("establishmentId")
		var employeeId string = c.Param("employeeId")
		var employee EmployeeStructure = getEmployee(establishmentId, employeeId)

		c.JSON(200, employee)
	})

	r.POST("/employee/:establishmentId", func(c *gin.Context) {

		var establishmentId string = c.Param("establishmentId")
		var employee EmployeeStructure
		err := c.Bind(&employee)

		if err != nil {
			log.Println(err)
		}

		employee.Id = "pro-" + uuid.NewV4().String()
		res := createEmployee(establishmentId, employee)

		c.JSON(200, res)
	})

	r.PATCH("/employee/:establishmentId", func(c *gin.Context) {

		var establishmentId string = c.Param("establishmentId")
		var employee EmployeeStructure

		err := c.Bind(&employee)
		if err != nil {
			log.Println(err)
		}

		res := updateEmployee(establishmentId, employee)

		c.JSON(200, res)
	})

	r.PATCH("/employee-fcm-token/:establishmentId/:employeeId", func(c *gin.Context) {

		var establishmentId string = c.Param("establishmentId")
		var employeeId string = c.Param("employeeId")
		var postBody struct {
			FcmToken string `json:"fcmToken"`
		}

		err := c.Bind(&postBody)
		if err != nil {
			log.Println(err)
		}

		response := saveFcmToken(establishmentId, employeeId, postBody.FcmToken)

		c.JSON(200, response)
	})

	r.POST("/sign-up/:establishmentId", func(c *gin.Context) {

		var msg string
		var err error
		var code int
		var establishmentId string = c.Param("establishmentId")
		var emailPassword struct {
			Email    string `json:"email,omitempty"`
			Password string `json:"password,omitempty"`
		}

		//prevents from unmarshalling json and binds the route param directly to the type
		err = c.Bind(&emailPassword)

		if err != nil {
			log.Println(err)
		} else {

			var employeeFromFirestore EmployeeStructure = getEmployeeByEmail(establishmentId, emailPassword.Email)
			msg = SignUp(employeeFromFirestore, emailPassword.Email, emailPassword.Password)

		}

		if err != nil {

			msg = err.Error()
			code = http.StatusInternalServerError

		} else if msg == "" {

			//UpdateShop(employees) //Insert the user's informations in elasticsearch todo : update the employee with password
			msg = "Success"
			code = http.StatusOK

		}

		c.JSON(code, msg)
	})

	r.GET("/apaleo-establishments/:code", HandlerApaleoConnect)

	err := r.Run()
	if err != nil {
		log.Print(err)
	}
}

func init() {
	initFortinbras()
	//all := getOneBrandAllEstablishments("MUC")
	//saveEstablishmentFromCsv("07042019establishments-en-london.csv")
	//addAllEstablishmentsAllEmployees()
}
