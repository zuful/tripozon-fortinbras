package main

import (
	"os"
	"time"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	fortinbras "bitbucket.org/zuful/tripozon-protobuf/fortinbras"
	"fmt"
)

const (
	address     = "localhost:50051"
)

func main(){

	//checkParams()
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := fortinbras.NewShopClient(conn)

	//insertDoc(c)
	getShops(c, "renting")
}

func checkParams()  {
	if len(os.Args) <= 1 {
		log.Fatal("Error: Action argument missing")
	} else if os.Args[1] == "create-company" {

		if len(os.Args) > 10 {
			log.Fatal("Error: Too many arguments")
		} else if len(os.Args) < 10 {
			log.Fatal("Error: Not enough arguments")
		}

	}
}

func insertDoc(c fortinbras.CompanyClient){
	// Contact the server and print out its response.
	var companyName string = "The Modern Hotel Montmartre"
	var shopAddress fortinbras.AddressStructure
	shopAddress.StreetName = "rue forest"
	shopAddress.StreetNumber = "3"
	shopAddress.ZipCode = "75018"
	shopAddress.City = "Paris"
	shopAddress.Region = "Île-de-France"
	shopAddress.Country = "France"

	var shop fortinbras.ShopStructure
	shop.Name = companyName + " - " + shopAddress.StreetName
	shop.Phone = "0504030201"
	shop.Address = &shopAddress
	shop.Tags = append(shop.Tags, "renting")

	var allShops []*fortinbras.ShopStructure = []*fortinbras.ShopStructure{&shop}

	var company fortinbras.CompanyStructure
	company.Name = companyName
	company.Email = "toto@tata.fr"
	company.Password ="password"
	company.Phone = "0102030405"
	company.Shop = allShops
	company.CreationDate = "26-03-2018"
	company.ModificationDate = "26-03-2018"

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.CreateCompany(ctx, &fortinbras.CreateCompanyRequest{
		Company: &company,
	})
	if err != nil {
		log.Fatalf("could not create company: %v", err)
	}

	fmt.Println("%v", r.IsCreated)
}

func getShops(c fortinbras.ShopClient, category string){
	ctx := context.Background()

	var city string = "paris"
	var geolocation fortinbras.GeolocationStructure = fortinbras.GeolocationStructure{
		Lat:49.052542,
		Lon:2.032574,
	}
	in := fortinbras.GetAllSearchedShopsRequest{
		Category:category,
		City:city,
		Geolocation: &geolocation,
	}
	r, err := c.GetAllSearchedShops(ctx, &in)
	if err != nil {
		log.Fatalf("could not get searched shops: %v", err)
	}

	fmt.Println("%v", r.AllShops)
}
