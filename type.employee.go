package main

type EmployeeStructure struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Firstname            string   `protobuf:"bytes,2,opt,name=firstname,proto3" json:"firstname,omitempty"`
	Lastname             string   `protobuf:"bytes,3,opt,name=lastname,proto3" json:"lastname,omitempty"`
	Email                string   `protobuf:"bytes,4,opt,name=email,proto3" json:"email,omitempty"`
	AvatarImgUrl         string   `protobuf:"bytes,5,opt,name=avatarImgUrl,proto3" json:"avatarImgUrl,omitempty"`
	EstablishmentId      string   `protobuf:"bytes,6,opt,name=establishmentId,proto3" json:"establishmentId,omitempty"`
	Role                 string   `protobuf:"bytes,7,opt,name=role,proto3" json:"role,omitempty"`
	CanChat              bool     `protobuf:"varint,8,opt,name=canChat,proto3" json:"canChat,omitempty"`
	EmailVerified        bool     `protobuf:"varint,9,opt,name=emailVerified,proto3" json:"emailVerified,omitempty"`
	FcmToken			 string   `json:"fcmToken,omitempty"`
	IsDisabled           bool     `protobuf:"varint,10,opt,name=isDisabled,proto3" json:"isDisabled,omitempty"`
	CreationDate         string   `protobuf:"bytes,11,opt,name=creationDate,proto3" json:"creationDate,omitempty"`
	ModificationDate     string   `protobuf:"bytes,12,opt,name=modificationDate,proto3" json:"modificationDate,omitempty"`
}
