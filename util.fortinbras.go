package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"github.com/satori/go.uuid"
	"io"
	"log"
	"os"
	"strings"
	"time"
)

func getEstablishementsFromCsvFile(filePath string) string {
	csvFile, _ := os.Open(filePath)
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var establishments []EstablishmentStructure
	var tags = make(map[string]bool)
	tags["hotel"] = true
	var firstLoop bool = true

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		if firstLoop == false {
			currentTime := time.Now().Local()
			var price = strings.Replace(line[4], "€", "", -1)
			price = strings.Replace(price, " ", "", -1)

			establishments = append(establishments, EstablishmentStructure{
				Name:  line[0],
				Phone: line[1],
				Email: line[2],
				Address: &AddressStructure{
					StreetName: line[3],
					ZipCode:    line[5],
					City:       strings.ToLower(line[6]),
					Country:    "France",
				},
				PriceStart: price,
				Website:    line[7],
				Description: Description{
					Fr: line[9],
					En: line[10],
				},
				Tags:             tags,
				CreationDate:     currentTime.Format("02-01-2006 15:04:05"),
				ModificationDate: currentTime.Format("02-01-2006 15:04:05"),
			})
		}

		firstLoop = false
	}
	establishmentsJson, _ := json.Marshal(establishments)
	fmt.Println(string(establishmentsJson))

	return string(establishmentsJson)
}

func saveEstablishmentFromCsv(filePath string) {

	var establishmentListJson string = getEstablishementsFromCsvFile(filePath)
	var establishmentList []*EstablishmentStructure
	err := json.Unmarshal([]byte(establishmentListJson), &establishmentList)
	if err != nil {
		fmt.Println(err)
	}

	for _, oneEstablishment := range establishmentList {
		oneEstablishment.Id = uuid.NewV4().String()
		//setEstablishmentGeoloc(oneEstablishment)

		createEstablishment(oneEstablishment)
	}

}
