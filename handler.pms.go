package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func HandlerApaleoConnect(c *gin.Context) {
	var code string = c.Param("code")

	statusCode, apaleoTokenJson := getApaleoTokenJson(code)
	if statusCode != http.StatusOK {
		c.String(statusCode, "Couldn't get token.\n")
		return
	}

	statusCode, apaleoEstablishmentJson := getApaleoEstablishmentsJson(apaleoTokenJson)
	if statusCode != http.StatusOK {
		c.String(statusCode, "Couldn't get the establishments.\n")
		return
	}

	statusCode, accountJson := getApaleoAccount(apaleoTokenJson)
	if statusCode != http.StatusOK {
		c.String(statusCode, "Couldn't get the account.\n")
		return
	}

	var allApaleoFormatedEstablishment []*EstablishmentStructure = mappingApaleoEstablishements(apaleoEstablishmentJson, accountJson)
	var allEstablishmentsFromDb []*EstablishmentStructure = getOneBrandAllEstablishments(allApaleoFormatedEstablishment[0].BrandId)

	if len(allEstablishmentsFromDb) == 0 {
		fmt.Println("Have to create the establishemnts in the database because they don't exist.")

		createPmsEstablishments(allApaleoFormatedEstablishment)
		allEstablishmentsFromDb = getOneBrandAllEstablishments(allApaleoFormatedEstablishment[0].BrandId)
	} else { // TODO: delete after test
		fmt.Println("No need to create the estalbishment because they already exist.")
	}

	c.JSON(http.StatusOK, allEstablishmentsFromDb)
}
