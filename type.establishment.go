package main

type EstablishmentStructure struct {
	Id                   string               `json:"id,omitempty"`
	BrandId              string               `json:"brandId,omitempty"`
	Name                 string               `json:"name,omitempty"`
	Etoiles              string               `json:"etoiles,omitempty"`
    Description          Description 		  `json:"description,omitempty"`
	Email                string               `json:"email,omitempty"`
	Phone                string               `json:"phone,omitempty"`
	Mobile               string               `json:"mobile,omitempty"`
	Website              string               `json:"website,omitempty"`
	Distance             float64              `json:"distance,omitempty"`
	Address              *AddressStructure    `json:"address,omitempty"`
	Employees            []*EmployeeStructure `json:"employees,omitempty"`
	PresentationImgUrl   string               `json:"presentationImgUrl,omitempty"`
	AlbumImgUrls         []string             `json:"albumImgUrls,omitempty"`
	CurrencyCode 		 string				  `json:"currencyCode,omitempty"`
	PriceStart         	 string               `json:"priceStart,omitempty"`
	Tags                 map[string]bool      `json:"tags,omitempty"`
	IsActive             bool                 `json:"isActive,omitempty"`
	CreationDate         string               `json:"creationDate,omitempty"`
	ModificationDate     string               `json:"modificationDate,omitempty"`
}

type Description struct {
	Fr string `json:"fr,omitempty"`
	En string `json:"en,omitempty"`
}
