package main

import (
	"cloud.google.com/go/firestore"
	"encoding/json"
	"firebase.google.com/go"
	auth2 "firebase.google.com/go/auth"
	"fmt"
	"github.com/kellydunn/golang-geo"
	"golang.org/x/net/context"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
	"log"
	"net/http"
)

var establishmentsCollectionName string = "establishments"
var employeesCollectionName string = "employees"

var app *firebase.App
var firebaseErr error
var authFirebase *auth2.Client
var firestoreClient *firestore.Client
var ctx context.Context

func initFortinbras() {

	//Connection to Firebase
	ctx = context.Background()
	opt := option.WithCredentialsFile("messenger-1670c.json")
	app, firebaseErr = firebase.NewApp(context.Background(), nil, opt)
	if firebaseErr != nil {
		log.Println(firebaseErr)
	}

	authFirebase, firebaseErr = app.Auth(context.Background())
	if firebaseErr != nil {
		log.Println(firebaseErr)
	}

	//Firestore
	firestoreClient, firebaseErr = app.Firestore(ctx)
	if firebaseErr != nil {
		log.Println(firebaseErr)
	}
}

func createEstablishment(establishment *EstablishmentStructure) *firestore.WriteResult {

	res, err := firestoreClient.Collection(establishmentsCollectionName).Doc(establishment.Id).Set(ctx, establishment)
	if err != nil {
		log.Println(err)
	}

	return res
}

func getEstablishment(establishmentId string) *EstablishmentStructure {

	var establishment *EstablishmentStructure
	establishmentSnap, err := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).Get(ctx)
	if err != nil {
		log.Println(err)
	}

	establishmentMap := establishmentSnap.Data()
	establishmentJson, err := json.Marshal(establishmentMap)
	if err != nil {
		log.Println(err)
	}

	err = json.Unmarshal([]byte(establishmentJson), &establishment)
	if err != nil {
		log.Println(err)
	}

	return establishment
}

func updatEstablishment(establishmentId string, establishment *EstablishmentStructure) *firestore.WriteResult {

	var allUpdates []firestore.Update = []firestore.Update{
		{Path: "Name", Value: establishment.Name},
		{Path: "Description.Fr", Value: establishment.Description.Fr},
		{Path: "Description.En", Value: establishment.Description.En},
		{Path: "Phone", Value: establishment.Phone},
		{Path: "Mobile", Value: establishment.Mobile},
		{Path: "Website", Value: establishment.Website},
		{Path: "Distance", Value: establishment.Distance},
		{Path: "Address", Value: establishment.Address},
		{Path: "PresentationImgUrl", Value: establishment.PresentationImgUrl},
		{Path: "AlbumImgUrls", Value: establishment.AlbumImgUrls},
		{Path: "PriceStart", Value: establishment.PriceStart},
		{Path: "Tags", Value: establishment.Tags},
		{Path: "ModificationDate", Value: establishment.ModificationDate},
	}

	res, err := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).Update(ctx, allUpdates)
	if err != nil {
		log.Println(err)
	}

	return res
}

//Fetches all the shops according to the tags given in parameter
func getSearchedEstablishment(category string, city string) []*EstablishmentStructure {

	var allEstablishments []*EstablishmentStructure = make([]*EstablishmentStructure, 0)

	res := firestoreClient.Collection(establishmentsCollectionName).
		Where("Tags."+category, "==", true).
		Where("Address.City", "==", city).Documents(ctx)

	defer res.Stop()
	for {
		var establishment *EstablishmentStructure
		establishmentSnap, err := res.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
		}

		establishmentMap := establishmentSnap.Data()
		establishmentJson, err := json.Marshal(establishmentMap)
		if err != nil {
			log.Println(err)
		}

		err = json.Unmarshal([]byte(establishmentJson), &establishment)
		if err != nil {
			log.Println(err)
		}

		allEstablishments = append(allEstablishments, establishment)
	}

	return allEstablishments
}

func setDistance(lat float64, lon float64, establishment *EstablishmentStructure) {

	myPoint := geo.NewPoint(lat, lon)
	shopPoint := geo.NewPoint(establishment.Address.Geolocation.Lat, establishment.Address.Geolocation.Lon)
	distance := myPoint.GreatCircleDistance(shopPoint)

	establishment.Distance = distance
}

func setEstablishmentGeoloc(establishment *EstablishmentStructure) {
	fmt.Println("%v", establishment.Address)
	var coordinates *geo.Point = getCoordinatesFromAdress(establishment.Address.StreetNumber + ", " + establishment.Address.StreetName + ", " + establishment.Address.City)

	var geolocation GeolocationStructure
	geolocation.Lat = coordinates.Lat()
	geolocation.Lon = coordinates.Lng()

	establishment.Address.Geolocation = &geolocation

}

func getCoordinatesFromAdress(address string) *geo.Point { //3, impasse de la planchette, paris

	var client http.Client
	geo.SetGoogleAPIKey("AIzaSyAwBDDctBFowuf_vqB5svSmPlYzyEuYdi8")
	geocoder := geo.GoogleGeocoder{
		HttpClient: &client,
	}

	coordinates, errGeocode := geocoder.Geocode(address)
	if errGeocode != nil {
		println(address)
		log.Println(errGeocode)
	}

	return coordinates
}

func createEmployee(establishmentId string, employee EmployeeStructure) *firestore.WriteResult {

	res, err := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).
		Collection(employeesCollectionName).Doc(employee.Id).Set(ctx, employee)
	if err != nil {
		log.Println(err)
	}

	return res
}

func getAllEmployees(establishmentId string) []EmployeeStructure {

	var allEmployees []EmployeeStructure = make([]EmployeeStructure, 0)

	res := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).
		Collection(employeesCollectionName).Documents(ctx)

	defer res.Stop()
	for {

		var employee EmployeeStructure
		employeeSnap, err := res.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
		}

		employeeMap := employeeSnap.Data()
		employeeJson, err := json.Marshal(employeeMap)
		if err != nil {
			log.Println(err)
		}

		err = json.Unmarshal([]byte(employeeJson), &employee)
		if err != nil {
			log.Println(err)
		}

		setEmployeeVerificationValues(employee)

		allEmployees = append(allEmployees, employee)

	}

	return allEmployees
}

func getEmployee(establishmentId string, employeeId string) EmployeeStructure {

	var employee EmployeeStructure

	res, err := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).
		Collection(employeesCollectionName).Doc(employeeId).Get(ctx)
	if err != nil { //check if there has been a problem with the query
		log.Println(err)
	}

	employeeMap := res.Data()
	establishmentJson, err := json.Marshal(employeeMap)
	if err != nil { //check if there has been a problem with marshaling the map to a json
		log.Println(err)
	}

	err = json.Unmarshal([]byte(establishmentJson), &employee)
	if err != nil { //check if there has been a problem with marshaling the json to a golang type
		log.Println(err)
	}

	setEmployeeVerificationValues(employee)

	return employee
}

func getEmployeeByEmail(establishmentId string, email string) EmployeeStructure {

	var employee EmployeeStructure

	res := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).
		Collection(employeesCollectionName).
		Where("Email", "==", email).Documents(ctx)

	defer res.Stop()

	for {

		employeeSnap, err := res.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
		}

		employeeMap := employeeSnap.Data()
		employeeJson, err := json.Marshal(employeeMap)
		if err != nil {
			log.Println(err)
		}

		err = json.Unmarshal([]byte(employeeJson), &employee)
		if err != nil {
			log.Println(err)
		}
	}

	setEmployeeVerificationValues(employee)

	return employee
}

func setEmployeeVerificationValues(employee EmployeeStructure) {

	//assign disable information from fire auth
	employeeFromFireAuth, err := getEmployeeFromFirebaseAuth(employee.Id)
	if err != nil {
		log.Println(err)
	}

	//for some reason if email verified or is disabled equal false the field won't appear on the result
	if employeeFromFireAuth != nil {
		employee.EmailVerified = employeeFromFireAuth.EmailVerified
		employee.IsDisabled = employeeFromFireAuth.Disabled
	} else {
		employee.IsDisabled = true
		employee.EmailVerified = false
	}

}

func updateEmployee(establishmentId string, employee EmployeeStructure) *firestore.WriteResult {

	var allUpdates []firestore.Update = []firestore.Update{
		{Path: "Firstname", Value: employee.Firstname},
		{Path: "Lastname", Value: employee.Lastname},
		{Path: "Email", Value: employee.Email},
		{Path: "AvatarImgUrl", Value: employee.AvatarImgUrl},
		{Path: "CanChat", Value: employee.CanChat},
		{Path: "ModificationDate", Value: employee.ModificationDate},
	}

	res, err := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).
		Collection(employeesCollectionName).Doc(employee.Id).Update(ctx, allUpdates)
	if err != nil {
		log.Println(err)
	}

	return res
}

func saveFcmToken(establishmentId string, employeeId string, fcmToken string) *firestore.WriteResult {

	var allUpdates []firestore.Update = []firestore.Update{
		{Path: "FcmToken", Value: fcmToken},
	}

	res, err := firestoreClient.Collection(establishmentsCollectionName).Doc(establishmentId).
		Collection(employeesCollectionName).Doc(employeeId).Update(ctx, allUpdates)
	if err != nil {
		log.Println(err)
	}

	return res
}

func SignUp(employeeFromFirestore EmployeeStructure, email string, password string) string {

	var msg string
	var err error

	// the employee has previously been created in db we create the employee in firebase
	if employeeFromFirestore.Email == email {

		var employeeToCreate *auth2.UserToCreate = new(auth2.UserToCreate)

		employeeToCreate.UID(employeeFromFirestore.Id)
		employeeToCreate.Email(email)
		employeeToCreate.Password(password)

		_, err = authFirebase.CreateUser(context.Background(), employeeToCreate)

		if err != nil {
			msg = err.Error()
		} else {
			msg = "Success"
		}

	} else {
		msg = "Email address or establishment not found"
	}

	return msg
}

func toggleEmployeeDisabled(employeeId string) bool {
	var employeeToUpdate auth2.UserToUpdate

	employee, err := authFirebase.GetUser(ctx, employeeId)
	if err != nil {
		log.Println(err)
	}

	if employee.Disabled == true {
		employeeToUpdate.Disabled(false)
	} else if employee.Disabled == false {
		employeeToUpdate.Disabled(true)
	}

	user, err := authFirebase.UpdateUser(ctx, employeeId, &employeeToUpdate)
	if err != nil {
		log.Println(err)
	}
	return user.Disabled
}

func getEmployeeFromFirebaseAuth(employeeId string) (*auth2.UserRecord, error) {
	employee, err := authFirebase.GetUser(ctx, employeeId)
	if err != nil {
		log.Println(err)
	}

	return employee, err
}
