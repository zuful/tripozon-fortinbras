package main

import (
	"encoding/json"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/api/iterator"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

func getApaleoEstablishmentsJson(jsonTokenJson string) (int, string) {
	var tokenObject struct{
		AccessToken string `json:"access_token"`
		ExpiresIn 	int		`json:"expires_in"`
		TokenType 	string `json:"token_type"`
	}
	data := url.Values{}

	var propertiesUrl = "https://api.apaleo.com/inventory/v1/properties"
	var err error = json.Unmarshal([]byte(jsonTokenJson), &tokenObject)
	CheckErr(err)

	client := &http.Client{}
	r, err := http.NewRequest("GET", propertiesUrl, strings.NewReader(data.Encode())) // URL-encoded payload
	CheckErr(err)

	r.Header.Add("Authorization", "Bearer " + tokenObject.AccessToken)
	r.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(r)
	CheckErr(err)
	defer resp.Body.Close()

	establishments, err := ioutil.ReadAll(resp.Body)
	CheckErr(err)

	return resp.StatusCode, string(establishments)
}

func getApaleoAccount(jsonTokenJson string) (int, string) {
	var tokenObject struct{
		AccessToken string `json:"access_token"`
		ExpiresIn 	int		`json:"expires_in"`
		TokenType 	string `json:"token_type"`
	}
	data := url.Values{}

	var propertiesUrl = "https://api.apaleo.com/account/v1/accounts/current"
	var err error = json.Unmarshal([]byte(jsonTokenJson), &tokenObject)
	CheckErr(err)

	client := &http.Client{}
	r, err := http.NewRequest("GET", propertiesUrl, strings.NewReader(data.Encode())) // URL-encoded payload
	CheckErr(err)

	r.Header.Add("Authorization", "Bearer " + tokenObject.AccessToken)
	r.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(r)
	CheckErr(err)
	defer resp.Body.Close()

	establishments, err := ioutil.ReadAll(resp.Body)
	CheckErr(err)

	return resp.StatusCode, string(establishments)
}

func getApaleoTokenJson(code string) (int, string) {
	var appaleoTokenUrl = "https://identity.apaleo.com/connect/token"

	data := url.Values{}
	data.Set("client_id", "TripozonConnect")
	data.Set("client_secret", "FtNdavAzzKqbCtwCQWGnhdc2Jt3t33")
	data.Set("grant_type", "authorization_code")
	data.Set("code", code)
	data.Set("redirect_uri", "https://tripozon-pro-admin.firebaseapp.com/")

	client := &http.Client{}
	r, err := http.NewRequest("POST", appaleoTokenUrl, strings.NewReader(data.Encode())) // URL-encoded payload
	CheckErr(err)
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(r)
	CheckErr(err)
	defer resp.Body.Close()

	token, err := ioutil.ReadAll(resp.Body)

	return resp.StatusCode, string(token)
}

func createPmsEstablishments(oneBrandAllEstablishmentsFromApi []*EstablishmentStructure) {
	for _, oneEstablishment := range oneBrandAllEstablishmentsFromApi {
		createEstablishment(oneEstablishment)
	}
}

func getOneBrandAllEstablishments(brandId string) []*EstablishmentStructure {
	var allEstablishments []*EstablishmentStructure = make([]*EstablishmentStructure, 0)

	res := firestoreClient.Collection(establishmentsCollectionName).
		Where("BrandId", "==", brandId).Documents(ctx)

	defer res.Stop()
	for {
		var establishment *EstablishmentStructure
		establishmentSnap, err := res.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Println(err)
		}

		establishmentMap := establishmentSnap.Data()
		establishmentJson, err := json.Marshal(establishmentMap)
		if err != nil {
			log.Println(err)
		}

		err = json.Unmarshal([]byte(establishmentJson), &establishment)
		if err != nil {
			log.Println(err)
		}

		allEstablishments = append(allEstablishments, establishment)
	}

	return allEstablishments
}

func mappingApaleoEstablishements(oneBrandAllEstablishmentsFromApiJson string, accountJson string) []*EstablishmentStructure {
	var allFormatedEstablishments []*EstablishmentStructure = make([]*EstablishmentStructure, 0)
	var oneBrandAllEstablishmentsFromApi struct{
		Properties []struct{
			Id string `json:"id"`
			Code string `json:"code"`
			Name string `json:"name"`
			Description string `json:"description"`
			Location struct{
				AddressLine1 string `json:"addressLine1"`
				PostalCode string `json:"postalCode"`
				City string `json:"city"`
				CountryCode string `json:"countryCode"`
			}`json:"location"`
			CurrencyCode string `json:"currencyCode"`
		} `json:"properties"`
		Count int `json:"count"`
	}
	err := json.Unmarshal([]byte(oneBrandAllEstablishmentsFromApiJson), &oneBrandAllEstablishmentsFromApi)
	CheckErr(err)

	var account struct {
		Code            string `json:"code,omitempty"`
		Name            string `json:"name,omitempty"`
		Description     string `json:"description,omitempty"`
		DefaultLanguage string `json:"defaultLanguage,omitempty"`
		LogoUrl         string `json:"logoUrl,omitempty"`
		Location        struct {
			AddressLine1 string `json:"addressLine1"`
			PostalCode   string `json:"postalCode"`
			City         string `json:"city"`
			CountryCode  string `json:"countryCode"`
		} `json:"location,omitempty"`
		Type string `json:"type"`
	}
	err = json.Unmarshal([]byte(accountJson), &account)
	CheckErr(err)

	for _, oneBrandOneEstablishmentsFromApi := range oneBrandAllEstablishmentsFromApi.Properties {
		var address AddressStructure = AddressStructure{
			StreetName:   oneBrandOneEstablishmentsFromApi.Location.AddressLine1,
			City:         strings.ToLower(oneBrandOneEstablishmentsFromApi.Location.City),
			ZipCode:      oneBrandOneEstablishmentsFromApi.Location.PostalCode,
			Country:      oneBrandOneEstablishmentsFromApi.Location.CountryCode,
		}

		var formatedEstablishment EstablishmentStructure = EstablishmentStructure{
			Id:uuid.NewV4().String(),
			Name: oneBrandOneEstablishmentsFromApi.Name,
			BrandId: account.Code,
			Description:Description{
				En: oneBrandOneEstablishmentsFromApi.Description,
			},
			Address: &address,
			IsActive: true,
			CurrencyCode: oneBrandOneEstablishmentsFromApi.CurrencyCode,
			PresentationImgUrl:account.LogoUrl,
			Tags: map[string]bool{"hotel": true},
		}

		allFormatedEstablishments = append(allFormatedEstablishments, &formatedEstablishment)
	}

	return allFormatedEstablishments
}
