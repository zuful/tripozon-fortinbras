package main

type AddressStructure struct {
	StreetName           string                `protobuf:"bytes,1,opt,name=streetName,proto3" json:"streetName,omitempty"`
	StreetNumber         string                `protobuf:"bytes,2,opt,name=streetNumber,proto3" json:"streetNumber,omitempty"`
	City                 string                `protobuf:"bytes,3,opt,name=city,proto3" json:"city,omitempty"`
	ZipCode              string                `protobuf:"bytes,4,opt,name=zipCode,proto3" json:"zipCode,omitempty"`
	Region               string                `protobuf:"bytes,5,opt,name=region,proto3" json:"region,omitempty"`
	Country              string                `protobuf:"bytes,6,opt,name=country,proto3" json:"country,omitempty"`
	Geolocation          *GeolocationStructure `protobuf:"bytes,7,opt,name=geolocation,proto3" json:"geolocation,omitempty"`
}

type GeolocationStructure struct {
	Lat                  float64  `protobuf:"fixed64,1,opt,name=lat,proto3" json:"lat,omitempty"`
	Lon                  float64  `protobuf:"fixed64,2,opt,name=lon,proto3" json:"lon,omitempty"`
}